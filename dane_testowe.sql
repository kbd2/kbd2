insert into USERS (PRIVILEGE_LEVEL,FIRSTNAME,LASTNAME,DOCUMENT_TYPE,DOCUMENT_ID,DB_USERNAME)
values ( 'S', 'JAN', 'KOWALSKI', 'ID', 'XYZ123456', 'KBD2A03');
iinsert into USERS (PRIVILEGE_LEVEL,FIRSTNAME,LASTNAME,DOCUMENT_TYPE,DOCUMENT_ID,DB_USERNAME)
values ('T', 'EWA', 'NOWAK', 'PA', '87654321', null);
insert into USERS (PRIVILEGE_LEVEL,FIRSTNAME,LASTNAME,DOCUMENT_TYPE,DOCUMENT_ID,DB_USERNAME)
values ('A', 'ANDRZEJ', 'PASTERZ', 'RC', '74185296', null);
insert into USERS (PRIVILEGE_LEVEL,FIRSTNAME,LASTNAME,DOCUMENT_TYPE,DOCUMENT_ID,DB_USERNAME)
values ('S', 'MAGDA', 'GESLER', 'RC', '74185666', null);
insert into USERS (PRIVILEGE_LEVEL,FIRSTNAME,LASTNAME,DOCUMENT_TYPE,DOCUMENT_ID,DB_USERNAME)
values ('T', 'MARCIN', 'CHUDY', 'RC', '74182296', null);
insert into USERS (PRIVILEGE_LEVEL,FIRSTNAME,LASTNAME,DOCUMENT_TYPE,DOCUMENT_ID,DB_USERNAME)
values ('S', 'KATARZYNA', 'SAWICKA', 'RC', '74185296', null);
insert into USERS (PRIVILEGE_LEVEL,FIRSTNAME,LASTNAME,DOCUMENT_TYPE,DOCUMENT_ID,DB_USERNAME)
values ('S', 'MICHAL', 'SUCHONSKI', 'RC', '74185296', null);

insert into ROOMS values ('161', 'T', 80, null);
insert into ROOMS values ('65', 'S', 30, 'Problems with projector reported');
insert into ROOMS values ('15', 'A', 5, 'Conference room');
insert into ROOMS values ('1', 'S', 100, null);
insert into ROOMS values ('AR', 'A', 200, null);

Insert into EQUIPMENT_TYPE values ('PR', 'PROJECTOR');
Insert into EQUIPMENT_TYPE values ('SC', 'SCREEN');
Insert into EQUIPMENT_TYPE values ('AC', 'AIR CONDITION');
Insert into EQUIPMENT_TYPE values ('SS', 'SOUND SYSTEM');

insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('PR', '65', 'Problems with this projector reported');
insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('PR', '161', null);
insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('AC', '161', null);
insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('SS', '161', null);
insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('SC', '161', null);
insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('SC', '65', null);
insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('AC', '15', null);
insert into EQUIPMENT(EQUIPMENT_TYPE_CODE, ROOM_CODE, COMMENTARY)
values ('AC', '65', 'is broken');


insert into RESERVATIONS values(161, 3, sysdate, sysdate+1 );
insert into RESERVATIONS values(15, 2, TO_DATE('08/09/2016', 'dd/mm/yyyy'), TO_DATE('08/09/2016', 'dd/mm/yyyy')+1/10 );
insert into RESERVATIONS values('AR', 3, TO_DATE('08/09/2016', 'dd/mm/yyyy'), TO_DATE('08/09/2016', 'dd/mm/yyyy')+1/10 );
insert into RESERVATIONS values('65', 1, TO_DATE('08/12/2016', 'dd/mm/yyyy'), TO_DATE('08/12/2016', 'dd/mm/yyyy')+1/10 );