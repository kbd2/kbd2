create or replace procedure startWWW as
begin
    htp.htmlOpen();
    htp.title('Landing Page');
    htp.bodyOpen();
    htp.div();
    htp.anchor('http://ora1.elka.pw.edu.pl:8080/kbd/web_plsql.unoccupied', 'Rooms available in choosen period(example for 22-01-2017 :  23-01-2017)');
    htp.div();
    htp.anchor('http://ora1.elka.pw.edu.pl:8080/kbd/web_plsql.reservations', 'Reservations made for choosen user(example for user_id = 1)');
    htp.bodyClose();
    htp.htmlClose();
end startWWW;

create or replace package web_plsql as
  procedure unoccupied(datefrom in varchar2 default '220120171200', dateto in varchar2 default '230120171400' );
  procedure reservations(userid in varchar2 default '1');
end web_plsql;
/
  create or replace package body web_plsql as
    -- example values for parameters
    -- datefrom = 081220160000 dateto = 081220160200, datefrom = 170320170300 dateto = 240320170300 
    procedure unoccupied(datefrom in varchar2 default '220120171200', dateto in varchar2 default '230120171400' ) is
    begin
    htp.htmlOpen();
    htp.title('Unoccupied rooms');
    htp.bodyOpen();
    htp.tableOpen('border="1"');
    htp.tableRowOpen();
    htp.tableHeader('ROOM_CODE');
    htp.tableRowClose();
    for idx in (SELECT * FROM ROOMS WHERE ROOM_CODE not in  
            (SELECT rom.ROOM_CODE FROM RESERVATIONS res JOIN ROOMS rom ON  res.ROOM_CODE = rom.ROOM_CODE 
            WHERE 
              (TO_DATE(datefrom, 'ddmmyyyyhh24mi') < DATE_TO AND TO_DATE(datefrom, 'ddmmyyyyhh24mi') > DATE_FROM)
              OR
              (TO_DATE(dateto, 'ddmmyyyyhh24mi') > DATE_FROM AND TO_DATE(dateto, 'ddmmyyyyhh24mi') < DATE_TO)
              )
              ) 
                loop
                htp.tableRowOpen();
                htp.tableData(idx.ROOM_CODE);
                htp.tableRowClose();
                end loop; 
        htp.tableClose();
        htp.bodyClose();
        htp.htmlClose();
    end;
    
    -- example values for parameters
    -- userid = 1, userid = 2, userid = 3
    procedure reservations(userid in varchar2 default '1') is
    begin
    htp.htmlOpen();
    htp.title('Users reservations');
    htp.bodyOpen();
    htp.tableOpen('border="1"');
    htp.tableRowOpen();
    htp.tableHeader('ROOM_CODE');
    htp.tableHeader('DATE_FROM');
    htp.tableHeader('DATE_TO');
    htp.tableRowClose();
    for idx in (
                select room_code, date_from, date_to from reservations where
                user_id = userid
              ) 
                loop
                htp.tableRowOpen();
                htp.tableData(idx.ROOM_CODE);
                htp.tableData(TO_CHAR(idx.DATE_FROM, 'DD/MM/YYYY:HH24:MI'));
                htp.tableData(TO_CHAR(idx.DATE_TO, 'DD/MM/YYYY:HH24:MI'));
                htp.tableRowClose();
                end loop; 
        htp.tableClose();
        htp.bodyClose();
        htp.htmlClose();
    end;
  end web_plsql;
  